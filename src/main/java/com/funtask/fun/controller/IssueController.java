package com.funtask.fun.controller;

import com.funtask.fun.common.CommonPage;
import com.funtask.fun.common.CommonResult;
import com.funtask.fun.entity.Issues;
import com.funtask.fun.http.request.Ids;
import com.funtask.fun.service.IIssuesService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 13:09
 * @description
 */
@RestController
@RequestMapping("/issues")
@Slf4j
public class IssueController {

    @Autowired
    private IIssuesService issuesService;

    @GetMapping("/list")
    public CommonPage<Issues> getIssues(Long projectId, Integer pageNum, Integer pageSize,
                                        String searchParam) {
        log.info("projectId: " + projectId + ", pageNum: " + pageNum + ", pageSize: " + pageSize + ", searchParam: " + searchParam);
        PageHelper.startPage(pageNum, pageSize);
        List<Issues> list = issuesService.getIssuesByPid(projectId,searchParam);
        PageInfo<Issues> pageInfo = new PageInfo<Issues>(list);
        return CommonPage.restPage(pageInfo);
    }

    @PostMapping("/add")
    public CommonResult<Boolean> addIssues(@RequestBody Issues issues) {
        boolean save = issuesService.createIssues(issues);
        return CommonResult.success(save);
    }

    @GetMapping("/get")
    public CommonResult<Issues> getIssue(String id) {
        Issues issue = issuesService.getById(id);
        return CommonResult.success(issue);
    }

    @PutMapping("/modify")
    public CommonResult<Boolean> modifyIssues(@RequestBody Issues issues) {
        boolean modify = issuesService.updateById(issues);
        return CommonResult.success(modify);
    }

    @DeleteMapping("/del")
    public CommonResult<Boolean> delIssues(Long id) {
        boolean remove = issuesService.removeById(id);
        return CommonResult.success(remove);
    }

    @DeleteMapping("/batchDel")
    public CommonResult<Boolean> batchDelIssues(@RequestBody Ids ids) {
        boolean removeBatch = issuesService.removeByIds(ids.getIds());
        return CommonResult.success(removeBatch);
    }

}
