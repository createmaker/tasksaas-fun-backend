package com.funtask.fun.controller;

import com.funtask.fun.common.CommonResult;
import com.funtask.fun.entity.Project;
import com.funtask.fun.service.IProjectService;
import com.funtask.fun.vo.ProjectDetailVo;
import com.funtask.fun.vo.ProjectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/1/4
 * @time 16:43
 * @description
 */
@RestController
@RequestMapping("/project")
public class ProjectController {
    @Autowired
    private IProjectService projectService;

    @GetMapping("/list")
    public CommonResult<List> projList(){
        List<ProjectVo> list = projectService.selectProjVos();
        return CommonResult.success(list);
    }

    @PostMapping("/add")
    public CommonResult<Boolean> addProject(@RequestBody Project project){
        boolean save = projectService.createProj(project);
        return CommonResult.success(save);
    }

    @GetMapping("/get")
    public CommonResult<Project> getProject(@RequestParam("id") Long id){
        return CommonResult.success(projectService.getById(id));
    }

    @GetMapping("/getDetailVo")
    public CommonResult<ProjectDetailVo> getProjectDetailVo(@RequestParam("id") Long id){
        return CommonResult.success(projectService.getDetailVoById(id));
    }


}
