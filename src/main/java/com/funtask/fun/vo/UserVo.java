package com.funtask.fun.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author zhangqi
 * @date 2024/2/18
 * @time 09:33
 * @description
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserVo {

    private Long id;

    private String nickName;

    private String sysAvatar;

    private String username;
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDatetime;
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String updateDatetime;

}
