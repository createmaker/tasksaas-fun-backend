package com.funtask.fun.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangqi
 * @date 2024/1/3
 * @time 13:58
 * @description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccessTokenVo {

    private String tokenHeader;
    private String token;
    private Long expiresIn;
}
