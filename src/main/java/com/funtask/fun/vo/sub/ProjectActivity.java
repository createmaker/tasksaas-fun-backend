package com.funtask.fun.vo.sub;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangqi
 * @date 2024/2/18
 * @time 13:03
 * @description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectActivity {

    private String username;
    private Long userId;

    private String activityType;

    private String content;
}
