package com.funtask.fun.vo.sub;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangqi
 * @date 2024/2/21
 * @time 10:13
 * @description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IssueTypePair {

    private Long typeId;
    private String typeName;
    private long[] counts;
}
