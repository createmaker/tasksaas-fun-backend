package com.funtask.fun.vo;

import com.funtask.fun.entity.Project;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangqi
 * @date 2024/1/5
 * @time 11:17
 * @description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectVo extends Project {

    private String creatorName;
    private String colorStr;
    //参与项目角色
    private String projectRole;
}
