package com.funtask.fun.vo;

import com.funtask.fun.entity.Project;
import com.funtask.fun.vo.sub.IssueTypePair;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/2/18
 * @time 09:17
 * @description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDetailVo extends Project {

    //任务数量
    private Integer[] tasksCount;
    //功能数量
    private Integer[] funcsCount;
    //bug数量
    private Integer[] bugsCount;
    //需求确认数量
    private Integer[] demandCount;

    private List<UserVo> userVoList;


    private List<IssueTypePair> issueTypePairs;
    //todo String -> activityObject
    private List<String> activityList;

}
