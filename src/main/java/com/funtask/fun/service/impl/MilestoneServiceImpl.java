package com.funtask.fun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.Milestone;
import com.funtask.fun.mapper.MilestoneMapper;
import com.funtask.fun.service.IMilestoneService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 19:07
 * @description
 */
@Service
@Slf4j
public class MilestoneServiceImpl extends ServiceImpl<MilestoneMapper, Milestone>
        implements IMilestoneService {
}
