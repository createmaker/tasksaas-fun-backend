package com.funtask.fun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.IssuesLog;
import com.funtask.fun.mapper.IssuesLogMapper;
import com.funtask.fun.service.IIssuesLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 19:26
 * @description
 */
@Service
@Slf4j
public class IssuesLogServiceImpl extends ServiceImpl<IssuesLogMapper, IssuesLog>
        implements IIssuesLogService {
}
