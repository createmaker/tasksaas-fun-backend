package com.funtask.fun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.GitInfoRelation;
import com.funtask.fun.mapper.GitInfoRelationMapper;
import com.funtask.fun.service.IGitInfoRelationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 11:27
 * @description
 */
@Service
@Slf4j
public class GitInfoRelationServiceImpl extends ServiceImpl<GitInfoRelationMapper, GitInfoRelation>
        implements IGitInfoRelationService {
}
