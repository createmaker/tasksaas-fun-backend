package com.funtask.fun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.Wiki;
import com.funtask.fun.mapper.WikiMapper;
import com.funtask.fun.service.IWikiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 11:32
 * @description
 */
@Service
@Slf4j
public class WikiServiceImpl extends ServiceImpl<WikiMapper, Wiki>
        implements IWikiService {
}
