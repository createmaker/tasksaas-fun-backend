package com.funtask.fun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.Module;
import com.funtask.fun.mapper.ModuleMapper;
import com.funtask.fun.service.IModuleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 11:30
 * @description
 */
@Service
@Slf4j
public class ModuleServiceImpl extends ServiceImpl<ModuleMapper, Module>
            implements IModuleService {
}
