package com.funtask.fun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.IssuesTag;
import com.funtask.fun.mapper.IssuesTagMapper;
import com.funtask.fun.service.IIssuesTagService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 10:37
 * @description
 */
@Service
@Slf4j
public class IssuesTagServiceImpl extends ServiceImpl<IssuesTagMapper, IssuesTag>
        implements IIssuesTagService {
    @Override
    public List<IssuesTag> getIssuesTagListByIssuesPk(Long issuesPk) {
        return baseMapper.getListByIssuesPk(issuesPk);
    }
}
