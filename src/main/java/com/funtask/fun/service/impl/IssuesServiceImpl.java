package com.funtask.fun.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.Issues;
import com.funtask.fun.entity.IssuesType;
import com.funtask.fun.enums.IssueStatus;
import com.funtask.fun.mapper.IssuesMapper;
import com.funtask.fun.mapper.IssuesTypeMapper;
import com.funtask.fun.service.IIssuesService;
import com.funtask.fun.utils.SecurityUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 13:09
 * @description
 */
@Service
@Slf4j
public class IssuesServiceImpl extends ServiceImpl<IssuesMapper, Issues>
        implements IIssuesService {
    @Autowired
    private IssuesTypeMapper issuesTypeMapper;
    @Override
    public List<Issues> getIssuesByPid(Long projectId,String searchParam) {
       return baseMapper.selectByProjectId(projectId,searchParam);
    }

    @Override
    public Boolean createIssues(Issues issues) {
        log.info("issues:{}",issues);
        IssuesType issuesType = issuesTypeMapper.selectById(issues.getIssuesTypeId());
        if (ObjectUtil.isNotEmpty(issuesType)){
            issues.setIssuesTypeStr(issuesType.getTitle());
        }
        issues.setCreatorId(SecurityUtil.getCurrentUser().getId());
        Long issueId = baseMapper.selectMaxIssueIdByProjectId(issues.getProjectId());
        issues.setIssueId(issueId);
        return super.save(issues);
    }

//    @Override
//    public Boolean changeIssues(Issues issues) {
//        if (issues.getIssuesTypeId())
//        return null;
//    }
}
