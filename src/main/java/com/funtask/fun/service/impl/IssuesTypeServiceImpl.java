package com.funtask.fun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.IssuesType;
import com.funtask.fun.mapper.IssuesTypeMapper;
import com.funtask.fun.service.IIssuesTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 19:11
 * @description
 */
@Service
@Slf4j
public class IssuesTypeServiceImpl extends ServiceImpl<IssuesTypeMapper, IssuesType>
    implements IIssuesTypeService {
}
