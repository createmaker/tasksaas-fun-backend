package com.funtask.fun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.IssuesReply;
import com.funtask.fun.mapper.IssuesReplyMapper;
import com.funtask.fun.service.IIssuesReplyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 19:31
 * @description
 */
@Service
@Slf4j
public class IssuesReplyServiceImpl extends ServiceImpl<IssuesReplyMapper, IssuesReply>
        implements IIssuesReplyService {
    @Override
    public List<IssuesReply> getIssuesReplyListByIssuesPk(Long issuesPk) {
        return baseMapper.getListByIssuesPk(issuesPk);
    }
}
