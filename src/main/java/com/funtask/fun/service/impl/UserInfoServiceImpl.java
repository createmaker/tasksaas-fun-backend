package com.funtask.fun.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.UserInfo;
import com.funtask.fun.manager.UserManager;
import com.funtask.fun.mapper.UserInfoMapper;
import com.funtask.fun.service.IUserInfoService;
import com.funtask.fun.vo.UserVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/1/3
 * @time 19:57
 * @description
 */
@Service
@Slf4j
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo>
        implements IUserInfoService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserManager userManager;
    @Override
    public Integer createUserInfo(UserInfo userInfo) {
        UserInfo existUser = userManager.getByUserName(userInfo.getUsername());
        if (existUser!=null){
            throw new RuntimeException("用户名重复");
        }
        userInfo.setPassword(passwordEncoder.encode(userInfo.getPassword()));
        return super.save(userInfo)?1:0;
    }
    @Override
    public Integer updateUserInfo(UserInfo userInfo) {
        if (StrUtil.isNotBlank(userInfo.getPassword())){
            userInfo.setPassword(passwordEncoder.encode(userInfo.getPassword()));
        }
        return super.updateById(userInfo)?1:0;
    }

    @Override
    public List<UserVo> findUserVoListByProjectId(Long projectId) {
        return null;
    }
}
