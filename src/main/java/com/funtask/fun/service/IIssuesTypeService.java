package com.funtask.fun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.funtask.fun.entity.IssuesType;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 19:11
 * @description
 */
public interface IIssuesTypeService extends IService<IssuesType> {
}
