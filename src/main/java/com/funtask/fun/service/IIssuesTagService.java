package com.funtask.fun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.funtask.fun.entity.IssuesTag;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 10:36
 * @description
 */
public interface IIssuesTagService extends IService<IssuesTag> {

    List<IssuesTag> getIssuesTagListByIssuesPk(Long issuesPk);
}
