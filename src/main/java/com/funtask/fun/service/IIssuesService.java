package com.funtask.fun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.funtask.fun.entity.Issues;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 13:09
 * @description
 */
public interface IIssuesService extends IService<Issues> {

    List<Issues> getIssuesByPid(Long projectId,String searchParam);

    Boolean createIssues(Issues issues);

//    Boolean changeIssues(Issues issues);


}
