package com.funtask.fun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.funtask.fun.entity.Wiki;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 11:32
 * @description
 */
public interface IWikiService extends IService<Wiki> {
}
