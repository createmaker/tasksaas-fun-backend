package com.funtask.fun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.funtask.fun.entity.Milestone;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 19:06
 * @description
 */
public interface IMilestoneService extends IService<Milestone> {
}
