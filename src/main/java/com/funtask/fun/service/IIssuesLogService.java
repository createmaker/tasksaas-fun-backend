package com.funtask.fun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.funtask.fun.entity.IssuesLog;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 19:19
 * @description
 */
public interface IIssuesLogService extends IService<IssuesLog> {
}
