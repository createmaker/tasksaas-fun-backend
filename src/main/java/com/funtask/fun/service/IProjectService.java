package com.funtask.fun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.funtask.fun.entity.Project;
import com.funtask.fun.vo.ProjectDetailVo;
import com.funtask.fun.vo.ProjectVo;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/1/4
 * @time 16:45
 * @description
 */
public interface IProjectService extends IService<Project> {

    Boolean createProj(Project project);
    List<ProjectVo> selectProjVos();

    ProjectDetailVo getDetailVoById(Long id);
}
