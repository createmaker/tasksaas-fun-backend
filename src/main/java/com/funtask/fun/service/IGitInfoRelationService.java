package com.funtask.fun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.funtask.fun.entity.GitInfoRelation;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 11:26
 * @description
 */
public interface IGitInfoRelationService extends IService<GitInfoRelation> {
}
