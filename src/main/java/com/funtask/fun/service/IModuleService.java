package com.funtask.fun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.funtask.fun.entity.Module;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 11:30
 * @description
 */
public interface IModuleService extends IService<Module> {
}
