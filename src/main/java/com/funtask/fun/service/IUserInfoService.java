package com.funtask.fun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.funtask.fun.entity.UserInfo;
import com.funtask.fun.vo.UserVo;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/1/3
 * @time 19:56
 * @description
 */
public interface IUserInfoService extends IService<UserInfo> {

   Integer createUserInfo(UserInfo userInfo);

   Integer updateUserInfo(UserInfo userInfo);

   List<UserVo> findUserVoListByProjectId(Long projectId);
}
