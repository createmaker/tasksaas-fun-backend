package com.funtask.fun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.funtask.fun.entity.IssuesReply;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 19:31
 * @description
 */
public interface IIssuesReplyService extends IService<IssuesReply> {
    List<IssuesReply> getIssuesReplyListByIssuesPk(Long issuesPk);
}
