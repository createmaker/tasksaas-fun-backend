package com.funtask.fun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.funtask.fun.entity.IssuesType;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 19:10
 * @description
 */
@Mapper
public interface IssuesTypeMapper extends BaseMapper<IssuesType> {
}
