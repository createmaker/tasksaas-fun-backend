package com.funtask.fun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.funtask.fun.entity.Project;
import com.funtask.fun.vo.ProjectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/1/5
 * @time 08:24
 * @description
 */
@Mapper
public interface ProjectMapper extends BaseMapper<Project> {

    List<ProjectVo> listProjectVos(@Param("id") Long id);
}
