package com.funtask.fun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.funtask.fun.entity.IssuesReply;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 19:31
 * @description
 */
@Mapper
public interface IssuesReplyMapper extends BaseMapper<IssuesReply> {
    List<IssuesReply> getListByIssuesPk(Long issuesPk);
}
