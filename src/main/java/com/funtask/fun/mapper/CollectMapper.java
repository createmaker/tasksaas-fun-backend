package com.funtask.fun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.funtask.fun.entity.Collect;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 11:22
 * @description
 */
@Mapper
public interface CollectMapper extends BaseMapper<Collect> {
}
