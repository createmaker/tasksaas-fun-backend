package com.funtask.fun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.funtask.fun.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhangqi
 * @date 2024/1/3
 * @time 19:52
 * @description
 */
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {


}
