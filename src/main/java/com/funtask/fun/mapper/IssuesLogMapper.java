package com.funtask.fun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.funtask.fun.entity.IssuesLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 19:19
 * @description
 */
@Mapper
public interface IssuesLogMapper extends BaseMapper<IssuesLog> {
}
