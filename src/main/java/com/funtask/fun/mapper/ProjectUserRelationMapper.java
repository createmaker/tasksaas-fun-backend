package com.funtask.fun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.funtask.fun.entity.ProjectUserRelation;
import com.funtask.fun.vo.UserVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/1/5
 * @time 13:42
 * @description
 */
@Mapper
public interface ProjectUserRelationMapper extends BaseMapper<ProjectUserRelation> {

    List<UserVo> searchUserVoListByProjectId(Long projectId);
}
