package com.funtask.fun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.funtask.fun.entity.IssuesTag;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 10:36
 * @description
 */
@Mapper
public interface IssuesTagMapper extends BaseMapper<IssuesTag> {
    List<IssuesTag> getListByIssuesPk(Long issuesPk);
}
