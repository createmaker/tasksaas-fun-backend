package com.funtask.fun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.funtask.fun.entity.Issues;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 17:06
 * @description
 */
@Mapper
public interface IssuesMapper extends BaseMapper<Issues> {
    List<Issues> selectByProjectId(@Param("projectId") Long projectId,
                                   @Param("searchParam") String searchParam);
    Long selectMaxIssueIdByProjectId(Long projectId);

}
