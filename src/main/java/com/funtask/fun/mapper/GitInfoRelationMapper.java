package com.funtask.fun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.funtask.fun.entity.GitInfoRelation;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 11:25
 * @description
 */
@Mapper
public interface GitInfoRelationMapper extends BaseMapper<GitInfoRelation> {
}
