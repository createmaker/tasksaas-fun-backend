package com.funtask.fun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.funtask.fun.entity.WorkRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 11:33
 * @description
 */
@Mapper
public interface WorkRecordMapper extends BaseMapper<WorkRecord> {
}
