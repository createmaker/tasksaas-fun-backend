package com.funtask.fun.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhangqi
 * @date 2024/1/3
 * @time 13:40
 * @description
 */
@AllArgsConstructor
@Getter
public enum LoginType {

    CODE("code","验证码登陆"),
    PASS("pass","密码登陆");

    private String code;
    private String desc;
}
