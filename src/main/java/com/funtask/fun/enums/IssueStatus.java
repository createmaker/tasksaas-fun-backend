package com.funtask.fun.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * @author zhangqi
 * @date 2024/2/18
 * @time 10:45
 * @description
 */
@Getter
@AllArgsConstructor
public enum IssueStatus {

    FRESH(1, "新建"),
    PROCESS(2, "处理中"),
    REOPEN(3, "重新打开"),
    FEEDBACK(4, "待反馈"),
    DONE(5, "已解决"),
    IGNORE(6, "已忽略"),
    CLOSE(7, "已关闭"),
    NONE(0, ""),
    ;
    private Integer code;
    private String remark;

    public static String getRemark(Integer code) {
       return Arrays.stream(IssueStatus.values())
                .filter(issueStatus -> issueStatus.getCode().equals(code))
                .findFirst().orElse(NONE).getRemark();
    }
}
