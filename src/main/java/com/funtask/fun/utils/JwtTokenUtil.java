package com.funtask.fun.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SecretKeyBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static io.jsonwebtoken.Jwts.SIG.HS512;

/**
 * @author zhangqi
 * @date 2024/1/3
 * @time 10:18
 * @description
 */
@Component
@Slf4j
public class JwtTokenUtil {

    private static final String CLAIM_KEY_USERNAME = "sub";

    private static final String CLAIM_KEY_CREATED = "created";
    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expiration}")
    private Long expiration;

    public Long getExpiration(){
        return expiration;
    }

    /**
     * 生成JWT token
     * @param claims
     * @return
     */
    private String generateToken(Map<String,Object> claims){
        byte[] keyBytes = Decoders.BASE64.decode(secret);
        Key key = Keys.hmacShaKeyFor(keyBytes);
        return Jwts.builder().signWith(key).claims(claims).expiration(new Date(System.currentTimeMillis()+expiration))
                .compact();
    }

    /**
     * 从token中获取JWT中的负载
     * @param token
     * @return
     */
    private Claims getClaimsFromToken(String token){
        Claims claims = null;
        byte[] bytes = Decoders.BASE64.decode(secret);
        Key key = Keys.hmacShaKeyFor(bytes);
        try {
            claims= Jwts.parser().verifyWith((SecretKey) key).build().parseSignedClaims(token)
                    .getPayload();
        } catch (JwtException | IllegalArgumentException e) {
           e.printStackTrace();
           log.info("JWT格式验证失败:{}",e.getMessage());
        }
        return claims;
    }

    /**
     * 从token中获取登陆id
     * @param token
     * @return
     */
    public Long getIdFromToken(String token){
        long id;
        try {
            Claims claims = getClaimsFromToken(token);
            id = Long.parseLong(claims.getSubject());
        } catch (Exception e) {
            e.printStackTrace();
            id = 0;
        }
        return id;
    }

    /**
     * 验证token是否还有效
     * @param token
     * @param userId
     * @return
     */
    public boolean validateToken(String token,Long userId){
        Long id = getIdFromToken(token);
        return Objects.equals(id, userId) && !isTokenExpired(token);
    }

    /**
     * 判断token是否已经失效
     * @param token
     * @return
     */
    private boolean isTokenExpired(String token) {
        Date date = new Date();
        Date expiredDate = getExpiredDateFromToken(token);
        return expiredDate.before(date);
    }

    /**
     * 从token中获取过期时间
     * @param token
     * @return
     */
    private Date getExpiredDateFromToken(String token) {
        Claims claims = getClaimsFromToken(token);
        return claims.getExpiration();
    }

    /**
     * 根据用户信息生成token
     * @param userId
     * @return
     */
    public String generateToken(Long userId){
        HashMap<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME,userId+"");
        claims.put(CLAIM_KEY_CREATED,new Date());
        return generateToken(claims);
    }

    /**
     * 判断token是否可以被刷新
     * @param token
     * @return
     */
    public boolean canRefresh(String token){
        return !isTokenExpired(token);
    }

    /**
     * 刷新token
     * @param token
     * @return
     */
    public String refreshToken(String token){
        Claims claims = getClaimsFromToken(token);
        claims.put(CLAIM_KEY_CREATED,new Date());
        return generateToken(claims);
    }


}
