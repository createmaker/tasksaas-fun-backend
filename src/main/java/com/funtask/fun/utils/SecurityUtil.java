package com.funtask.fun.utils;

import com.funtask.fun.components.MyUserDetails;
import com.funtask.fun.model.CurrentUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Currency;

/**
 * @author zhangqi
 * @date 2024/1/5
 * @time 13:56
 * @description
 */
@Slf4j
public class SecurityUtil {

    public static CurrentUser getCurrentUser(){
        SecurityContext context = SecurityContextHolder.getContext();
        Object principal = context.getAuthentication().getPrincipal();
        log.info("principal:{}",principal);
        MyUserDetails userDetails =(MyUserDetails)principal;
        CurrentUser currentUser = CurrentUser.builder().id(userDetails.getUserId())
                .username(userDetails.getUsername())
                .build();
        log.info("getCurrentUser:{}",currentUser);
        return currentUser;
    }


}
