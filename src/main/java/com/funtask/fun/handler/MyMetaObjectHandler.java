package com.funtask.fun.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "createDatetime", Date.class, new Date());
        this.strictInsertFill(metaObject, "latestUpdateDatetime", Date.class, new Date());
        this.strictInsertFill(metaObject, "updateDatetime", Date.class, new Date());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "updateDatetime", Date.class, new Date());
        this.strictUpdateFill(metaObject, "latestUpdateDatetime", Date.class, new Date());
    }
}
