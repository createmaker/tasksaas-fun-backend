package com.funtask.fun.manager;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.InfoLog;
import com.funtask.fun.mapper.InfoLogMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 11:24
 * @description
 */
@Component
@Slf4j
public class InfoLogManager extends ServiceImpl<InfoLogMapper, InfoLog> {
}
