package com.funtask.fun.manager;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.Project;
import com.funtask.fun.mapper.ProjectMapper;
import org.springframework.stereotype.Component;

/**
 * @author zhangqi
 * @date 2024/1/5
 * @time 08:25
 * @description
 */
@Component
public class ProjManager extends ServiceImpl<ProjectMapper, Project> {
}
