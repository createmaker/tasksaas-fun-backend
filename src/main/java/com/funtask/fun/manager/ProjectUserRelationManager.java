package com.funtask.fun.manager;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.ProjectUserRelation;
import com.funtask.fun.entity.UserInfo;
import com.funtask.fun.mapper.ProjectUserRelationMapper;
import com.funtask.fun.vo.UserVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/1/5
 * @time 13:39
 * @description
 */
@Component
@Slf4j
public class ProjectUserRelationManager extends ServiceImpl<ProjectUserRelationMapper, ProjectUserRelation> {

    public Boolean saveRelation(Long userId,Long projId){
        ProjectUserRelation projectUserRelation = ProjectUserRelation.builder().projectId(projId).userId(userId).build();
        return super.save(projectUserRelation);
    }

    public List<UserVo> getUserVoListByProjectId(Long projectId){
        return baseMapper.searchUserVoListByProjectId(projectId);
    }

}
