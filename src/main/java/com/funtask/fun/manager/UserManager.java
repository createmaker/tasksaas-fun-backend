package com.funtask.fun.manager;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.demos.web.User;
import com.funtask.fun.entity.UserInfo;
import com.funtask.fun.mapper.UserInfoMapper;
import com.funtask.fun.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhangqi
 * @date 2024/1/3
 * @time 13:08
 * @description
 */
@Component
public class UserManager extends ServiceImpl<UserInfoMapper,UserInfo> {


    public UserInfo getByUserName(String username){
        return super.lambdaQuery().eq(UserInfo::getUsername,username).one();
    }

    public UserInfo getUserById(Long id){
        return super.lambdaQuery().eq(UserInfo::getId,id).one();
    }


}
