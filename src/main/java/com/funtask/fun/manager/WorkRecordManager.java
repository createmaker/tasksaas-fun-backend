package com.funtask.fun.manager;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.funtask.fun.entity.WorkRecord;
import com.funtask.fun.mapper.WorkRecordMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zhangqi
 * @date 2024/2/5
 * @time 11:33
 * @description
 */
@Service
@Slf4j
public class WorkRecordManager extends ServiceImpl<WorkRecordMapper, WorkRecord> {
}
