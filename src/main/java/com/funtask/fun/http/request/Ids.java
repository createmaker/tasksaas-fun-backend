package com.funtask.fun.http.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author zhangqi
 * @date 2024/2/21
 * @time 10:57
 * @description
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Ids {
    @NotEmpty(message = "ids can not be empty")
    private List<Long> ids;
}
