package com.funtask.fun.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangqi
 * @date 2024/1/5
 * @time 14:09
 * @description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CurrentUser {

    private Long id;
    private String username;

}
