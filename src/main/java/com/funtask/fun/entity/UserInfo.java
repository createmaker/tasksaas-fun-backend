package com.funtask.fun.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.funtask.fun.vo.UserVo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author zhangqi
 * @date 2024/1/3
 * @time 13:12
 * @description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("web_userinfo")
public class UserInfo {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String username;
    private String password;
    private String mobilePhone;
    private String nickName;
    private String email;

    private String gitUsername;

    private String gitAvatar;

    private String gitPassword;

    private BigDecimal forwardScore;

    private String wechatOpenid;

    private String wechatUnionid;

    private String wechatAvatar;

    private String wechatNickname;

    private Long avatarFrameWearingId;

    private String sysAvatar;


    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDatetime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateDatetime;

    private transient List<SimpleGrantedAuthority> authList;


}
