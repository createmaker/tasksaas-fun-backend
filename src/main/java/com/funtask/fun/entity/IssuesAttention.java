package com.funtask.fun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 13:28
 * @description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("web_issues_attention")
public class IssuesAttention {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long issuesId;

    private Long userinfoId;

    
}
