package com.funtask.fun.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 13:29
 * @description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("web_issuestag")
public class IssuesTag {
    @TableId(type = IdType.AUTO)
    private Long id;

    private String tName;
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDatetime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date latestUpdateDatetime;
    private Long creatorId;
}
