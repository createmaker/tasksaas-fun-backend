package com.funtask.fun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangqi
 * @date 2024/2/4
 * @time 13:29
 * @description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("web_issuestype")
public class IssuesType {
    @TableId(type = IdType.AUTO)
    private Long id;

    private String title;
    private Long projectId;
}
